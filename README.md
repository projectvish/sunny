# README #

**Sunny** is a Real time Weather App developed in Android platform having the feature for location-awareness.

### How do I get set up? ###

Open Android Studio

Import the project into Android Studio. [File->New->Import Project]
Please use your API Keys for Forecast and Google API clients

**Modify API Keys, compile and run**

To set up a sample:

1.	Create a Google Developer account if you do not have one and make note of the Google API Key which is a Unique Key generated for your account.

2.	Modify AndroidManifest.xml and place your Google API Key there under metadata, so that the Google Play services library for the app works.


3.	Create an account in Forecast.io if you do not have one and make note of the Forecast API Key which is a Unique Key generated for your account. This is used to retrieve the Real time Weather forecast data.

4.	Modify the ui/MainActivity.java and place your Forecast API Key in the getforecast() function, so that the current Weather forecast data is retrieved in JSON format. 

Compile and run.


* Configuration- Android Studio IDE, Genymotion (Simulator)
* Dependencies- 
                
        OkHTTP library for network access

		Butterknife library for Field and method binding for Android Views

		Google Play services library for use of Google API

		Forecast API is used for retrieving of Real time Weather forecast data in JSON format

Note*: Please use your API Keys for Forecast and Google API clients